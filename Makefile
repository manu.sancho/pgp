# grep the version from the mix file
APP_NAME=pgp
APP_VERSION := $(shell cat VERSION)
SERVER_IP=82.223.216.95
DOCKERHUB_USER=simplymanu


# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help dist push

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help


# DOCKER TASKS
# Build the container
build: ## Build Docker container.
	docker build --file "docker/Dockerfile" --tag $(DOCKERHUB_USER)/$(APP_NAME):$(APP_VERSION) .

build-nc: ## Build Docker container without caching.
	docker build --no-cache --file "docker/Dockerfile" --tag $(DOCKERHUB_USER)/$(APP_NAME):$(APP_VERSION) .

fetch_db_prod: ## Copy remote PRODUCTION DB to local temporary folder
	scp root@$(SERVER_IP):/var/$(APP_NAME)/prod/db/db.sqlite3 ./tmp/db/db.sqlite3

fetch_db_staging: ## Copy remote STAGING DB to local temporary folder
	scp root@$(SERVER_IP):/var/$(APP_NAME)/staging/db/db.sqlite3 ./tmp/db/db.sqlite3

migrate_db: ## Apply pending migrations on local DB
	cd src;	python manage.py migrate ; cd ..

dist: ## Export image to a tar file.
	docker save pgp:$(APP_VERSION) --output dist/docker-pgp-$(APP_VERSION).tar

push_db_staging: ## Upload local DB to staging remote folder
	scp ./tmp/db/db.sqlite3 root@$(SERVER_IP):/var/$(APP_NAME)/staging/db/db.sqlite3

push_db_prod: ## Upload local DB to staging remote folder
	scp ./tmp/db/db.sqlite3 root@$(SERVER_IP):/var/$(APP_NAME)/prod/db/db.sqlite3

push_docker_staging: ## Push image to DockerHub for STAGING
	docker login
	docker tag $(DOCKERHUB_USER)/$(APP_NAME):$(APP_VERSION) $(DOCKERHUB_USER)/$(APP_NAME):staging
	docker push $(DOCKERHUB_USER)/$(APP_NAME):staging

push_docker_prod: ## Push image to DockerHub for PRODUCTION
	docker login
	docker tag $(DOCKERHUB_USER)/$(APP_NAME):$(APP_VERSION) $(DOCKERHUB_USER)/$(APP_NAME):production
	docker push $(DOCKERHUB_USER)/$(APP_NAME):production