import os

from django.core.exceptions import *

try:
    from .base_settings import *
except ImportError:
    raise FieldDoesNotExist

DEBUG = True
THUMBNAIL_DEBUG = True
MODELTRANSLATION_DEBUG = True


ALLOWED_HOSTS = ['*']


# **************************************************************************
# Database
# **************************************************************************

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(TMP_DIR, 'db', 'db.sqlite3'),
    }
}


# **************************************************************************
# UPLOADED FILES
# **************************************************************************
# Uploaded file will be put into and fetched from this folder during development
MEDIA_ROOT = os.path.join(TMP_DIR,'uploads')
