from django.core.exceptions import *

try:
    from .base_settings import *
except ImportError:
    raise FieldDoesNotExist

DEBUG = False
THUMBNAIL_DEBUG = False
MODELTRANSLATION_DEBUG = False


# **************************************************************************
# Security settings
# **************************************************************************
# Setup support for proxy headers
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
ALLOWED_HOSTS = ['*']


# **************************************************************************
# Database
# **************************************************************************

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/var/db/db.sqlite3',
    }
}


# **************************************************************************
# STATIC FILES
# **************************************************************************
# Static files (CSS, JavaScript, Images)
# WHERE to PUT static files in. Static files will be served from this path
# Path relative to Django root folder
STATIC_ROOT = '/srv/static'



# **************************************************************************
# UPLOADED FILES
# **************************************************************************
# Where to PUT uploaded files in. Static files will be fetch from this path
# not used with Amazon S3!
MEDIA_ROOT = '/srv/uploads'