
import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

SINO = (
    (False, 'No'),
    (True, 'Sí'),
)

##########################################
# AUXILIAR MODELS
##########################################

class CasualtyType(models.Model):
    name = models.CharField(
        verbose_name=_("Tipo de siniestro"),
        max_length=50,
        blank=False,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(
        verbose_name=_("Orden"),
        default=0,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % (
            self.name,
        )

    class Meta:
        verbose_name = "Tipo de siniestro"
        verbose_name_plural = "Tipos de siniestros"
        ordering = ['weight', 'name', ]


class JobVisit(models.Model):
    """
    Store a job visit
    """
    job = models.ForeignKey(
        'Job',
        verbose_name="Job",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    visit_date = models.DateField(
        verbose_name="Fecha de la visita",
        null=True,
        blank=True,
    )
    visit_notes = models.TextField(
        verbose_name="Notas de la visita",
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s - %s' % (
            self.job.__str__(),
            self.visit_date,
        )

    class Meta:
        verbose_name = "Visita"
        verbose_name_plural = "Visitas"
        ordering = ['visit_date', ]


class Ramo(models.Model):
    name = models.CharField(
        verbose_name=_("Nombre"),
        max_length=50,
        blank=False,
        null=True,
    )
    code = models.CharField(
        verbose_name=_("Código"),
        max_length=3,
        blank=False,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(
        verbose_name=_("Orden"),
        default=0,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % (
            self.name,
        )

    class Meta:
        verbose_name = "Ramo"
        verbose_name_plural = "Ramos"
        ordering = ['weight', 'name', ]


class JobType(models.Model):
    name = models.CharField(
        verbose_name=_("Tipo de encargo"),
        max_length=50,
        blank=False,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(
        verbose_name=_("Orden"),
        default=0,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % (
            self.name,
        )

    class Meta:
        verbose_name = "Tipo de encargo"
        verbose_name_plural = "Tipos de encargos"
        ordering = ['weight', 'name', ]


class JobStatus(models.Model):
    name = models.CharField(
        verbose_name="Nombre del estado",
        help_text="A ser posible usa un participio",
        max_length=60,
        blank=True,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(
        verbose_name=_("Orden"),
        default=0,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = "Estado de un encargo"
        verbose_name_plural = "Estados de los encargos"
        ordering = ['weight']


class Client(models.Model):
    name = models.CharField(
        verbose_name=_("Nombre"),
        max_length=50,
        blank=False,
        null=False,
    )
    acronym = models.CharField(
        verbose_name=_("Acrónimo"),
        max_length=5,
        blank=True,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(
        verbose_name=_("Orden"),
        default=0,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
        ordering = ['weight', 'name', ]


class Job(models.Model):

    """
    Model to store a Job
    """

    # Job related fields
    job_year = models.PositiveIntegerField(
        blank=True,
        null=True,
    )
    job_id = models.PositiveIntegerField(
        blank=True,
        null=True,
    )
    job_uid = models.CharField(
        max_length=8,
        verbose_name="Referencia del encargo",
        null=False,
        unique=True,
    )
    job_type = models.ForeignKey(
        'JobType',
        verbose_name="Tipo de encargo",
        on_delete=models.CASCADE,
        blank=False,
        default=1,
        null=True,
    )
    client = models.ForeignKey(
        'Client',
        verbose_name="Cliente",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    job_status = models.ForeignKey(
        'JobStatus',
        verbose_name="Estado del encargo",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    entry_date = models.DateField(
        verbose_name="Fecha de entrada",
        null=True,
        blank=True,
    )
    update_date = models.DateField(
        verbose_name="Modificado",
        null=True,
        blank=True,
        auto_now=True,
    )
    due_date = models.DateField(
        verbose_name="Fecha límite",
        null=True,
        blank=True,
    )
    close_date = models.DateField(
        verbose_name="Fecha de cierre",
        null=True,
        blank=True,
    )

    # casualty related fields
    case_number = models.CharField(
        max_length=255,
        verbose_name="Nº de expediente",
        blank=True,
        null=True,
    )
    casualty_type = models.ForeignKey(
        'CasualtyType',
        verbose_name="Tipo de siniestro",
        on_delete=models.CASCADE,
        blank=True,
        null=True,        
    )
    casualty_date = models.DateField(
        verbose_name="Fecha del siniestro",
        null=True,
        blank=True,
    )
    casualty_address = models.TextField(
        verbose_name="Dirección del siniestro",
        blank=True,
        null=True,
    )
    casualty_city = models.CharField(
        max_length=255,
        verbose_name="Población del siniestro",
        blank=True,
        null=True,
    )

    # insurance policy fields
    insurance_policy_number = models.CharField(
        max_length=50,
        verbose_name="Nº de póliza",
        blank=True,
        null=True,
    )
    ramo = models.ForeignKey(
        'Ramo',
        verbose_name="Ramo",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    policyholder_name = models.CharField(
        max_length=50,
        verbose_name="Nombre del asegurado",
        blank=True,
        null=True,
    )
    policyholder_phone = models.CharField(
        max_length=50,
        verbose_name="Teléfono del asegurado",
        blank=True,
        null=True,
    )
    policyholder_email = models.CharField(
        max_length=50,
        verbose_name="Email del asegurado",
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s' % (
            self.pk,
        )

    class Meta:
        verbose_name = "Encargo"
        verbose_name_plural = "Encargos"
        ordering = ['-job_uid', ]

    def get_next_uid(self):
        now = datetime.datetime.now()
        cur_year = int(now.strftime('%y'))
        last_job = Job.objects.filter(job_year=cur_year).order_by('-job_id').first()
        if last_job:
#            print ("Last id: " + str(last_job.job_id))
            next_id = last_job.job_id + 1
#            print ("Next id: " + str(next_id))
        else:
            next_id = 1
        
        next_uid = '%s-%s' % (
            cur_year,
            str(next_id).zfill(4)  # fills string with leading zeros until 4 digits
        )

        return cur_year, next_id, next_uid

    def save(self, *args, **kwargs):
        """
        Custom actions on model save
        """
        # Auto generate Job unique ID
        # YY-XXXX where YY is year and XXXX a sequential ID        
        if not self.id:  # Newly created object
            year, id, uid = self.get_next_uid()
            self.job_id = id
            self.job_year = year
            self.job_uid = uid
    
        super(Job, self).save(*args, **kwargs)


class Comment(models.Model):
    """
    Store a comment
    """
    job = models.ForeignKey(
        'Job',
        verbose_name="Job",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    author = models.ForeignKey(
        User,
        verbose_name="Autor",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    date = models.DateField(
        verbose_name="Fecha",
        auto_now=True,
    )
    comment = models.TextField(
        verbose_name="Comentario",
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s - Comment %s' % (
            self.job.__str__(),
            self.date,
        )

    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "Comentarios" 