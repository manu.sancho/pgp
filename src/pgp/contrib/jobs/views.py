# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView

from pgp.contrib.jobs.models import Job


class JobView(DetailView): 

    model = Job
    template_name = 'jobs/job.html'
    object_name = 'job'

