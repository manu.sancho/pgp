from django.contrib import admin
from django.forms import Textarea, TextInput 
from django.utils.html import format_html
from django.utils.translation import gettext as _
from django.db.models import Count
from django.db.models import ForeignKey
from django.urls import reverse
from django.utils.text import slugify


from easy_select2 import Select2
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from django_admin_listfilter_dropdown.filters import DropdownFilter, RelatedDropdownFilter
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from import_export.admin import ImportExportModelAdmin


from import_export import resources

from .models import *
from .resources import *


def custom_slugify(txt):
        """A custom version of slugify that retains non-ascii characters. The purpose of this
        function in the application is to make URLs more readable in a browser, so there are 
        some added heuristics to retain as much of the title meaning as possible while 
        excluding characters that are troublesome to read in URLs. For example, question marks 
        will be seen in the browser URL as %3F and are thereful unreadable. Although non-ascii
        characters will also be hex-encoded in the raw URL, most browsers will display them
        as human-readable glyphs in the address bar -- those should be kept in the slug."""
        txt = txt.replace('º',' ').replace('ª',' ').replace('/',' ')
        txt = slugify(txt)
        txt = txt.replace('-','+')
        return txt


class JobFieldAdmin(SortableAdminMixin, admin.ModelAdmin):
    
    fields = (
        'name', 
    )
    ordering = ('weight', )
    sortable_field_name = "weight" 
    show_full_result_count = False
    view_on_site = False
    massadmin_exclude = [
        'name',
        'weight',
    ]


class JobVisitInline(admin.TabularInline):
    model = JobVisit
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':2})},
    }
    extra = 1


class CommentInline(admin.TabularInline):
    model = Comment
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':3})},
    }
    extra = 1
    readonly_fields = ['author', ]
    fields = ['author', 'comment']

    def save_model(self, request, obj, form, change):
        # add user as author
        obj.author = request.user
        obj.save()


@admin.register(Job)
class JobAdmin(ImportExportModelAdmin):

    change_list_template = 'admin/jobs/job/change_list.html'
    resource_class = JobResource
    formfield_overrides = {
        ForeignKey: {'widget': Select2()},
        models.TextField: {'widget': Textarea(attrs={'rows':3})},
    }        
    list_display = (
#        'job_year',
#        'job_id',
        'get_job_uid',
        'case_number',
        'get_casualty_date',
        'client',
        'get_address',
        'get_entry_date',
        'get_close_date',
        'job_status',
    )
    search_fields = (
        'job_uid',
        'job_type__name', 
        'client__name', 
        'job_status__name', 
        'entry_date',
        'due_date',
        'close_date',
        'case_number', 
        'casualty_type__name',
        'casualty_date',
        'casualty_address',
        'casualty_city',
        'insurance_policy_number', 
        'ramo__name', 
        'policyholder_name', 
        'policyholder_phone', 
        'policyholder_email',
    )
    show_full_result_count = False
    list_filter = (
        'job_type',
        ('casualty_type',  RelatedDropdownFilter),
        'client',
        'job_status',
    )
    readonly_fields = ['job_uid']
    fieldsets = (
        ("", {
            'fields': (
                'job_uid',
                'job_type', 
                'client', 
                'job_status', 
                'entry_date',
                'due_date',
                'close_date',
                )
        }),
        ("Siniestro", {
            'fields': (
                'case_number', 
                'casualty_type',
                'casualty_date',
                'casualty_address',
                'casualty_city',
                )
        }),
        ("Póliza", {
            'fields': (
                'insurance_policy_number', 
                'ramo', 
                'policyholder_name', 
                'policyholder_phone', 
                'policyholder_email',
                )
        }),
    )
    radio_fields = {
        'job_type': admin.HORIZONTAL,
    }
    view_on_site = False
    inlines = [JobVisitInline, CommentInline]
    massadmin_exclude = [
        'case_number',
    ]
    
    def get_edit_button(self, obj):
        return format_html('<a class="btn btn-primary" target="_blank" href="%s"><i class="icon-pencil"></i> %s</a>' % (
            reverse('admin:jobs_job_change', args=(obj.id,)),
            'Editar',
        ))

    get_edit_button.short_description = ''

    def get_job_uid(self, obj):
        return obj.job_uid
    get_job_uid.short_description = 'Referencia'

    def get_casualty_date(self, obj):
        return obj.casualty_date
    get_casualty_date.short_description = 'Siniestro'

    def get_entry_date(self, obj):
        return obj.entry_date
    get_entry_date.short_description = 'Entrada'

    def get_close_date(self, obj):
        return obj.close_date
    get_close_date.short_description = 'Cierre'

    def get_address(self, obj):
        if obj.casualty_address:
            full_address = '%s, %s' % (obj.casualty_address, obj.casualty_city)
            slugged_address = custom_slugify(obj.casualty_address)
            if obj.casualty_city:
                slugged_address += ', ' + custom_slugify(obj.casualty_city)

            url = 'https://www.google.es/maps/place/' + slugged_address
            output = '<a target="_blank" href="%s">%s</a>' % (url, full_address)

            return format_html(output)        

    get_address.short_description = 'Dirección'


@admin.register(Ramo)
class RamoAdmin(JobFieldAdmin):
    fields = (
        'name',
        'code',
    )


@admin.register(JobVisit)
class JobVisitAdmin(ImportExportModelAdmin):

    resource_class = JobVisitResource
    view_on_site = False


@admin.register(Comment)
class CommentAdmin(ImportExportModelAdmin):

    resource_class = CommentResource
    view_on_site = False


@admin.register(JobType)
class JobTypeAdmin(JobFieldAdmin):
    pass


@admin.register(JobStatus)
class JobStatusAdmin(JobFieldAdmin):
    pass


@admin.register(CasualtyType)
class JCasualtyTypeAdmin(JobFieldAdmin):
    pass


@admin.register(Client)
class JCasualtyTypeAdmin(JobFieldAdmin):
    fields = (
        'name',
        'acronym',
    )