
import datetime

from rest_framework import serializers, viewsets
from rest_framework.serializers import HyperlinkedRelatedField
from pgp.contrib.jobs.models import *
from pgp.contrib.jobs.views import *


# Serializers define the API representation.
class JobTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobType
        fields = '__all__'


class CasualtyTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = CasualtyType
        fields = '__all__'


class JobVisitSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobVisit
        fields = '__all__'


class RamoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ramo
        fields = '__all__'


class JobStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobStatus
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'


class JobSerializer(serializers.HyperlinkedModelSerializer):

    job_uid = serializers.CharField(max_length=10,required=True)
    job_type = JobTypeSerializer()
    client = ClientSerializer()
    job_status = JobStatusSerializer()
    entry_date = serializers.DateField(initial=datetime.date.today)
    update_date = serializers.DateField(initial=datetime.date.today)
    due_date = serializers.DateField()
    close_date = serializers.DateField()

    case_number = serializers.CharField()
    casualty_type = CasualtyTypeSerializer()
    casualty_address = serializers.CharField()
    casualty_city = serializers.CharField()
    insurance_policy_number = serializers.CharField()
    ramo = RamoSerializer()
    policyholder_name = serializers.CharField()
    policyholder_phone = serializers.CharField()
    policyholder_email = serializers.EmailField()

    class Meta:
        model = Job
        fields = '__all__'


class JobViewSet(viewsets.ModelViewSet):
    """
    Viewset to list all Jobs
    """
    lookup_field = ('job_uid')
    queryset = Job.objects.all()
    serializer_class = JobSerializer