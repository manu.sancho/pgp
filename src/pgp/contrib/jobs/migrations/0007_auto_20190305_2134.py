# Generated by Django 2.1.1 on 2019-03-05 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0006_auto_20190304_2212'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='weight',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='Orden'),
        ),
        migrations.AddField(
            model_name='jobvisit',
            name='weight',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='Orden'),
        ),
        migrations.AlterField(
            model_name='jobstatus',
            name='weight',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='Orden'),
        ),
        migrations.AlterField(
            model_name='jobtype',
            name='weight',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='Orden'),
        ),
        migrations.AlterField(
            model_name='ramo',
            name='weight',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True, verbose_name='Orden'),
        ),
    ]
