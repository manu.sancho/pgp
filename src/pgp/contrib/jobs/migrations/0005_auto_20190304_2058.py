# Generated by Django 2.1.1 on 2019-03-04 20:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('jobs', '0004_auto_20190218_2300'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now=True, verbose_name='Fecha')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='Comentario')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Autor')),
                ('job', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='jobs.Job', verbose_name='Job')),
            ],
            options={
                'verbose_name': 'Comentario',
                'verbose_name_plural': 'Comentarios',
            },
        ),
        migrations.AlterModelOptions(
            name='casualtytype',
            options={'ordering': ['name'], 'verbose_name': 'Tipo de siniestro', 'verbose_name_plural': 'Tipos de siniestros'},
        ),
        migrations.AlterModelOptions(
            name='ramo',
            options={'ordering': ['weight', 'name'], 'verbose_name': 'Ramo', 'verbose_name_plural': 'Ramos'},
        ),
    ]
