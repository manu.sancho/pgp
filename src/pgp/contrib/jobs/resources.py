from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from .models import *


class GetOrCreateForeignKeyWidget(ForeignKeyWidget):
    """
    Import/export resource widget for Foreign Key fields.
    Get related foreign key. Create it if does not exists.

    Return: valied model instance or None
    """

    def clean(self, value, row=None, *args, **kwargs):

        if not value or value == None:
            return None

        kwargs = {'%s' % self.field: value}

        return self.model.objects.get_or_create(**kwargs)[0]


class JobVisitResource(resources.ModelResource):

    job = fields.Field(
        column_name='job',
        attribute='job',
        widget=GetOrCreateForeignKeyWidget(Job, 'job_uid')
    )

    class Meta:
        model = JobVisit
        import_id_fields = [
            'job',
            'visit_date',
        ]


class CommentResource(resources.ModelResource):

    job = fields.Field(
        column_name='job',
        attribute='job',
        widget=GetOrCreateForeignKeyWidget(Job, 'job_uid')
    )

    class Meta:
        model = Comment
        import_id_fields = [
            'job',
            'date',
            'author',
            'comment',
        ]


class JobResource(resources.ModelResource):

    job_type = fields.Field(
        column_name='job_type',
        attribute='job_type',
        widget=GetOrCreateForeignKeyWidget(JobType, 'name')
    )

    job_status = fields.Field(
        column_name='job_status',
        attribute='job_status',
        widget=GetOrCreateForeignKeyWidget(JobStatus, 'name')
    )

    casualty_type = fields.Field(
        column_name='casualty_type',
        attribute='casualty_type',
        widget=GetOrCreateForeignKeyWidget(CasualtyType, 'name')
    )

    client = fields.Field(
        column_name='client',
        attribute='client',
        widget=GetOrCreateForeignKeyWidget(Client, 'name')
    )

    ramo = fields.Field(
        column_name='ramo',
        attribute='ramo',
        widget=GetOrCreateForeignKeyWidget(Ramo, 'name')
    )

    class Meta:
        model = Job
        skip_unchanged = True
        exclude = ('updated_date', )
        import_id_fields = (
            'job_type',
            'job_status',
            'casualty_type',
            'client',
            'ramo',
            'job_uid',
            'entry_date',
            'due_date',
            'close_date',
            'case_number',
            'casualty_date',
            'casualty_address',
            'casualty_city',
            'insurance_policy_number',
            'policyholder_name',
            'policyholder_phone',
            'policyholder_email',
        )
